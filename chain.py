class SomeObject:
    
    def __init__(self):
        self.integer_field = 0
        self.float_field = 0.0
        self.string_field = ""


class NullHandler:
    
    def __init__(self, successor=None):
        self.__successor = successor
        
    def handle(self, someobject, event):
        if self.__successor is not None:
            return self.__successor.handle(someobject, event)


class EventGet:
    
    def __init__(self, var_type):
        self.var_type = var_type

        
class EventSet:
    
    def __init__(self, value):
        self.value = value


class IntHandler(NullHandler):
    
    def handle(self, someobject, event):
        if event.__class__.__name__ == 'EventGet' and event.var_type == int:
            return someobject.integer_field
        elif event.__class__.__name__ == 'EventSet' and type(event.value) == int:
            someobject.integer_field = event.value
        else:
            return super().handle(someobject, event)
            

class FloatHandler(NullHandler):
    
    def handle(self, someobject, event):
        if event.__class__.__name__ == 'EventGet' and event.var_type == float:
            return someobject.float_field
        elif event.__class__.__name__ == 'EventSet' and type(event.value) == float:
            someobject.float_field = event.value
        else:
            return super().handle(someobject, event)
            
            
class StrHandler(NullHandler):
    
    def handle(self, someobject, event):
        if event.__class__.__name__ == 'EventGet' and event.var_type == str:
            return someobject.string_field
        elif event.__class__.__name__ == 'EventSet' and type(event.value) == str:
            someobject.string_field = event.value
        else:
            return super().handle(someobject, event)
