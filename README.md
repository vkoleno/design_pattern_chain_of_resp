Реализовать Chain of Responsibility


Вам дан объект класса SomeObject, содержащего три поля: integer_field, float_field и string_field:

class SomeObject:

    def __init__(self):
        self.integer_field = 0
        self.float_field = 0.0
        self.string_field = ""

Необходимо реализовать:

EventGet(<type>) создаёт событие получения данных соответствующего типа
EventSet(<value>) создаёт событие изменения поля типа type(<value>)
Необходимо реализовать классы NullHandler, IntHandler, FloatHandler, StrHandler так, чтобы можно было создать цепочку:

chain = IntHandler(FloatHandler(StrHandler(NullHandler())))

1. chain.handle(obj, EventGet(int)) — вернуть значение obj.integer_field
2. chain.handle(obj, EventGet(str)) — вернуть значение obj.string_field
3. chain.handle(obj, EventGet(float)) — вернуть значение obj.float_field
4. chain.handle(obj, EventSet(1)) — установить значение obj.integer_field =1
5. chain.handle(obj, EventSet(1.1)) — установить значение obj.float_field = 1.1
6. chain.handle(obj, EventSet("str")) — установить значение obj.string_field = "str"
